import os
import pyodbc
import pandas as pd
import numpy as np

## customize to your working enviroment
wdir = '.'
ddir = './data'
os.makedirs(ddir, exist_ok=True)

#### pyodbc connnection
## choose one approporiate to your working environment
conn = pyodbc.connect("DRIVER={SQL Server};"
                  "SERVER=cvgs070073;"
                  "DATABASE=HSBINT"
                  )
####################################
dsn = 'cvgs070073'
uid = 'am.munichre.com\\yourid'
pwd = 'yourpasswd'
conn_str = f'DSN={dsn};UID={uid};PWD={pwd}'
conn = pyodbc.connect(conn_str)


###############################
#### load data sets
### policy data
tbl_pol = '[HSBINT].[dbo].[EPL_POLICIES_18]'
qry_str = 'select * from %s' % tbl_pol
epl_pols = pd.read_sql(qry_str, conn)

### experian data
tbl_exp = '[RISKEXT].[dbo].[Experian_Score]'
qry_str = 'select T1.* from %s T1 right join (select Experian_Score_Id from %s where Experian_Score_Id is not NULL) T2 on T1.Experian_Score_Id=T2.Experian_Score_Id' % (tbl_exp, tbl_pol)
epl_exp = pd.read_sql(qry_str, conn).drop_duplicates()

## collect experian data used in the previous model
cursor = conn.cursor()
tbl_exp = '[RISKEXT].[dbo].[Experian_Score]'
qry_str = f'select distinct T1.Company_Name, T1.Company_Address, T1.Company_City, T1.Company_State, T1.Company_Zip into ##TMP01 from {tbl_exp} T1 right join (select Experian_Score_Id from {tbl_pol} where Experian_Score_Id is not NULL) T2 on T1.Experian_Score_Id=T2.Experian_Score_Id'
cursor.execute(qry_str)
cursor.execute('commit;')
qry_str = f'select T2.* into ##TMP02 from ##TMP01 as T1, {tbl_exp} as T2 where T1.Company_Name=T2.Company_Name and T1.Company_Address=T2.Company_Address and T1.Company_City=T2.Company_City and T1.Company_State=T2.Company_State and T1.Company_Zip=T2.Company_Zip'
cursor.execute(qry_str)
cursor.execute('commit;')

qry_str = f'select * from ##TMP02'
exp_scores = pd.read_sql(qry_str, conn)

#################################
#### update experian scores of exposures
#### strategy: using Company_name and address of collected Experain scores, find the closest to 3rd quarter of policy year
epl_pols['Experian_Score_Id'] = [str(int(x)) if pd.notnull(x) else '' for x in epl_pols.Experian_Score_Id]
exp_scores['Experian_Score_Id'] = exp_scores.Experian_Score_Id.astype(str)
## join exposures with Experian scores
epl_pols = pd.merge(
    epl_pols,
    exp_scores[['Experian_Score_Id','Company_Name','Company_Address','Company_City','Company_State','Company_Zip']],
    on='Experian_Score_Id', how='left')
epl_pols['pid'] = range(epl_pols.shape[0])
epl = epl_pols.loc[epl_pols.Experian_Score_Id !=''][['pid','PolYR','Company_Name','Company_Address','Company_City','Company_State','Company_Zip']] # 313975

## cleaning up an Experian score pool
scores = exp_scores[['Experian_Score_Id', 'Company_Name', 'Company_Address', 'Company_City', 'Company_State', 'Company_Zip',
            'Date_Of_Data']].copy() # 5335924
scores['exp_yr'] = scores.Date_Of_Data.dt.year
scores = scores.loc[~((scores.Company_Name.str.strip()=='') & (scores.Company_Address.str.strip()=='') & (scores.Company_City.str.strip()=='') & (scores.Company_State.str.strip()==''))]  #1317577

epl_scores_cands = pd.merge(epl, scores, how='left', on=['Company_Name','Company_Address','Company_City','Company_State','Company_Zip']) # 2095090
epl_scores_cands = epl_scores_cands.loc[pd.notnull(epl_scores_cands.PolYR)] # 2095090
epl_scores_cands = epl_scores_cands.loc[pd.notnull(epl_scores_cands.exp_yr)] # 2034461

## start matching
pid_lst, scid_lst = [], []
for idx,df in epl_scores_cands.groupby('pid'):
    print(idx)
    yr_dif = np.abs(df.PolYR - df.exp_yr)
    sdf = df[yr_dif==min(yr_dif)] # try to match to policy year
    if sdf.shape[0]>1:
        t0 = pd.Timestamp('%d-10-01' % df.PolYR.iloc[0])  # tyr to match to 3rd quarter
        sdf = sdf.iloc[[np.abs(sdf.Date_Of_Data - t0).values.argmin()]]
    pid = sdf.pid.iloc[0]
    scid = sdf.Experian_Score_Id.iloc[0]
    pid_lst.append(pid)
    scid_lst.append(scid)

new_scid_tbl = pd.DataFrame({'pid': pid_lst, 'Experian_Score_Id_new': scid_lst})

epl_pols_new_exp = pd.merge(epl_pols, new_scid_tbl, how='left', on='pid')
epl_pols_new_exp[['Experian_Score_Id','Experian_Score_Id_new']]
epl_pols_new_exp['Experian_Score_Id_new'] = np.where(pd.notnull(epl_pols_new_exp.Experian_Score_Id_new),epl_pols_new_exp.Experian_Score_Id_new,epl_pols_new_exp.Experian_Score_Id)
epl_pols_new_exp = epl_pols_new_exp.drop(columns=['Company_Name','Company_Address','Company_City','Company_State','Company_Zip','pid'])
epl_pols_new_exp.to_pickle(os.path.join(ddir,'epl_pols_new_exp.pk'))



def summary_of_new_match():
    epl_pols = pd.read_pickle(os.path.join(ddir, 'epl_pols_new_exp.pk'))
    exp_scores01 = pd.read_pickle(os.path.join(ddir, 'epl_exp.pk'))
    exp_scores02 = pd.read_pickle(os.path.join(ddir, 'exp_scores.pk'))

    def get_summary(epl_pols, vn, exp_scores):
        polyrs = epl_pols[['PolYR',vn]].rename(columns={vn: 'Experian_Score_Id'})
        scs = exp_scores[['Experian_Score_Id','Date_Of_Data']].copy()
        scs['ExpYR'] = scs.Date_Of_Data.dt.year
        scs['Experian_Score_Id'] = scs['Experian_Score_Id'].astype(str)
        df = pd.merge(polyrs, scs, how='left', on='Experian_Score_Id')

        df['same_pol_exp_yr'] = np.where(df.PolYR == df.ExpYR, 'yes','no')
        df.loc[pd.isnull(df.ExpYR),'same_pol_exp_yr'] = 'na'

        total_smry_tbl = df.groupby('same_pol_exp_yr').same_pol_exp_yr.count()
        pd.pivot_table(df, values='same_pol_exp_yr', index='PolYR', aggfunc=lambda x: len(x))
        df['cnt'] = 1
        yearly_smry_tbl = pd.pivot_table(df, index='PolYR', columns='same_pol_exp_yr', values='cnt', aggfunc=sum)

        return total_smry_tbl, yearly_smry_tbl

    vn = 'Experian_Score_Id'; exp_scores = exp_scores01
    res1 = get_summary(epl_pols, 'Experian_Score_Id', exp_scores01)
    res2 = get_summary(epl_pols, 'Experian_Score_Id_new', exp_scores02)

    total_smry = pd.concat([res1[0].rename('Experian_Score_Id'), res2[0].rename('Experian_Score_Id_new')], axis=1)

    colidx_levels = [['Experian_Score_Id', 'Experian_Score_Id_new'], res1[1].columns.tolist()]
    colidx = pd.MultiIndex.from_product(colidx_levels)
    yearly_smry = pd.concat([res1[1], res2[1]], axis=1)
    yearly_smry.columns = colidx


    print(total_smry)
    print(yearly_smry)
#################################################################################################################


def upload_to_sql():
    from sqlalchemy import create_engine

    epl_pols = pd.read_pickle(os.path.join(ddir, 'epl_pols_new_exp.pk'))  # 1,872,985
    epl_pols['seqid'] = range(1, epl_pols.shape[0]+1)
    epl_pols.to_pickle(os.path.join(ddir, 'epl_pols_new_exp_seqid.pk'))

    # sqlalchemy
    conn_str = f'mssql+pyodbc://{uid}:{pwd}@{dsn}'
    engine = create_engine(conn_str)

    #### create a view of policies by adding a seq id
    conn_str = f'DSN={dsn};UID={uid};PWD={pwd}' # pyodbc
    conn = pyodbc.connect(conn_str)
    cursor = conn.cursor()
    cursor.execute('USE HSBINT;')
    cursor.execute('select seqid=IDENTITY(INT,1,1), * into #mytmp01 from dbo.EPL_POLICIES_18')
    cursor.execute('select * into dbo.EPL_POLICIES_18_seqid from #mytmp01 order by seqid')
    cursor.execute('commit;')
    cursor.close()
    conn.close()

    #### upload Experian_Score_Id_new
    exp_sid_new = epl_pols[['seqid','Experian_Score_Id_new']].query('Experian_Score_Id_new!=""')
    exp_sid_new.to_pickle(os.path.join(ddir,'exp_sid_new.pk'))
    exp_sid_new = pd.read_pickle(os.path.join(ddir,'exp_sid_new.pk'))

    # method1 - use pandas : slow
    conn_str = f'mssql+pyodbc://{uid}:{pwd}@{dsn}'
    engine = create_engine(conn_str)
    engine.execute('USE HSBINT;')
    exp_sid_new.to_sql('EPL_POLICIES_18_Experian_Score_Id_new',con=engine,schema='dbo',index=False)

    # method2 - pyodbc : slow
    conn_str = f'DSN={dsn};UID={uid};PWD={pwd}'
    conn = pyodbc.connect(conn_str)
    cursor = conn.cursor()

    for i,record in enumerate(exp_sid_new.to_records(index=False).tolist()): #313975
        print(i)
        cursor.execute('insert into HSBINT.dbo.EPL_POLICIES_18_Experian_Score_Id_new01 (seqid,Experian_Score_Id_new) values ({},{})'.format(*record))

    cursor.execute('commit;')
    cursor.close()
    conn.close()

    # method3 - use SQL management studio : fast

    ################################
    ## create a view
    conn_str = f'DSN={dsn};UID={uid};PWD={pwd}'
    conn = pyodbc.connect(conn_str)
    cursor = conn.cursor()
    cursor.execute('USE HSBINT;')
    cursor.execute('CREATE VIEW vw_EPL_POLICIES_18_Experian_Score_Id_new as (select T0.*, T1.Experian_Score_Id_new from HSBINT.dbo.EPL_POLICIES_18_seqid as T0 left join HSBINT.dbo.EPL_POLICIES_18_Experian_Score_Id_new01 as T1 on T0.seqid=T1.seqid )')
    cursor.execute('COMMIT;')
    ###################################
    ## or run this in sql management studio
    ## use HSBINT;
    ## GO
    ## create view dbo.vw_EPL_POLICIES_18_Experian_Score_Id_new AS
    ##  (select T0.*, T1.Experian_Score_Id_new
    ##      from HSBINT.dbo.EPL_POLICIES_18_seqid as T0
    ##      left join HSBINT.dbo.EPL_POLICIES_18_Experian_Score_Id_new01 as T1
    ##      on T0.seqid=T1.seqid )
    #########################################
    ## result is HSBINT.dbo.vw_EPL_POLICIES_18_Experian_Score_Id_new


