import pandas as pd
import numpy as np
import pyodbc
import os
import re

'''
## The original code allows many-to-many relationships from claims to locations
## and there needs be a post process to change to many-to-one relationship.
## This code uses "Loss Date" but the original code uses "Reported date".
## Parantheses are added to a set of punctuation makrs to remove in comparing two text strings
## Comments: this python code will be very slow because it contains substring searches.
'''


## customize to your working environment
ddir = '/home/xpanda/datadir/epl_models_20180820/DATA_ETL/data'
os.makedirs(ddir, exist_ok=True)


#### pyodbc connnection
## choose one approporiate to your working environment
conn = pyodbc.connect("DRIVER={SQL Server};"
                  "SERVER=cvgs070073;"
                  "DATABASE=HSBINT"
                  )
####################################
dsn = 'cvgs070073'
uid = 'am.munichre.com\\yournid'
pwd = 'yourpwd'
conn_str = f'DSN={dsn};UID={uid};PWD={pwd}'
conn = pyodbc.connect(conn_str)


#################
#### Load data
tbl = 'HSBINT.dbo.EPL_POLICIES_18'
pols = pd.read_sql(f'select * from {tbl}', conn)
pols.to_pickle(os.path.join(ddir,'EPL_POLICIES_18'))
pols = pd.read_pickle(os.path.join(ddir,'EPL_POLICIES_18')).reset_index(drop=True)
clms = pd.read_excel(os.path.join(ddir,'claims_combined_gwcc_s3-warehouse.xlsx')).reset_index(drop=True)

## add unique id for each row
clms['clm_seqid'] = range(clms.shape[0])
pols['pol_seqid'] = range(pols.shape[0])

## value and type conversion
pols['Treaty_No'] = pols['Treaty_No'].astype(str).str.strip()
clms['HSB Policy Number'] = clms['HSB Policy Number'].astype(str).str.strip()
clms['CC Policy Number'] = clms['CC Policy Number'].astype(str).str.strip()
pols['policy_eff_dt'] = pd.DatetimeIndex(pols.policy_eff_dt)
pols['policy_exp_dt'] = pd.DatetimeIndex(pols.policy_exp_dt)




##########################################
### Precedure 1
## match by policy number and treaty number
'''
    t1.HSB_Policy_Number = t2.Treaty_No
    AND
    (
            INDEX(
                SUBSTR(
                    COMPRESS(
                        UPPER(t1.CC_Policy_Number),
                        ',.#;:"/-@! $%&*+_'
                    ),
                    1,10
                ),
                SUBSTR(
                    COMPRESS(
                        UPPER(t2.Client_Policy),
                        ',.#;:"/-@! $%&*+_'
                    ),
                    1,10
                )
            ) > 0 OR

            INDEX(
                SUBSTR(
                    COMPRESS(
                        UPPER(t2.Client_Policy),
                        ',.#;:"/-@! $%&*+_'
                    ),
                    1,10
                ),
                SUBSTR(
                    COMPRESS(
                        UPPER(t1.CC_Policy_Number),
                        ',.#;:"/-@! $%&*+_'
                    ),
                    1,10
                )
            ) > 0
    ) AND
		t1.Date_Claim_Reported between t2.policy_eff_dt and t2.policy_exp_dt
'''
#### mod
## "Date Claim Reported" is replaced by "Date Of Loss"
## "Only consider cases of len(cc_pol_nbr)>0

chars_to_remove = r'[,\.#;:"\-\@! \$%&\*\+_\(\)\\]'
pols['cc_pol_nbr'] = pols['Client_Policy'].str.replace(chars_to_remove,'',regex=True).str.strip().str.slice(0,10).str.upper()
clms['cc_pol_nbr'] = clms['CC Policy Number'].str.replace(chars_to_remove,'',regex=True).str.strip().str.slice(0,10).str.upper()

assert np.where(pd.isnull(clms['HSB Policy Number']))[0].shape[0]==0
assert np.where(clms['HSB Policy Number']=='')[0].shape[0]==0

match_tbl = []
pols_pool = pols[pols.cc_pol_nbr.apply(len)>0]
for idx,clm in clms.iterrows():
    print(idx)
    if len(clm.cc_pol_nbr)==0: continue
    cond1 = (pols_pool.Treaty_No == clm['HSB Policy Number'])
    cond2 = (pols_pool.policy_eff_dt <= clm['Date Of Loss']) & (pols_pool.policy_exp_dt > clm['Date Of Loss']) # clm['Date Calim Reported'] replaced
    pols_cand = pols_pool[cond1 & cond2]
    if pols_cand.shape[0]==0: continue
    pols_cand = pols_cand.iloc[
        np.where(
            [re.search(clm.cc_pol_nbr,pol.cc_pol_nbr) if len(clm.cc_pol_nbr)<len(pol.cc_pol_nbr)
             else re.search(pol.cc_pol_nbr,clm.cc_pol_nbr) for _,pol in pols_cand.iterrows()]
        )[0]
    ].copy()
    if pols_cand.shape[0]>0:
        pols_cand['clm_seqid'] = clm.clm_seqid
        match_tbl.append(pols_cand)

pols = pols.drop(['cc_pol_nbr'],axis=1)

if len(match_tbl)>0: # there is any improvment
    pols_matched = pd.concat(match_tbl).drop(['cc_pol_nbr'], axis=1)
    v_match_gwcc = pd.merge(clms, pols_matched, how='left', on='clm_seqid')
    v_match_gwcc.to_pickle(os.path.join(ddir,'v_match_gwcc.pk'))

    tmp = v_match_gwcc[pd.notnull(v_match_gwcc.pol_seqid)][['clm_seqid','pol_seqid']]
    tmp['pol_seqid'] = tmp.pol_seqid.astype(int)
    print(tmp.clm_seqid.drop_duplicates().shape[0] / clms.shape[0]) # hit ratio 0.497
    print(tmp.groupby('clm_seqid').count().sort_values('pol_seqid', ascending=False)[:10])

    ## send the unmatched results from treaty and policy to unmatched 1
    matched_1 = v_match_gwcc[pd.notnull(v_match_gwcc.pol_seqid)]
    unmatched_clms1 = clms[~clms.clm_seqid.isin(pols_matched.clm_seqid.unique())].copy()
else:
    print('No match')
    matched_1 = pd.DataFrame()
    unmatched_clms1 = clms.copy()

matched_1.to_pickle(os.path.join(ddir, 'matched_1.pk'))
unmatched_clms1.to_pickle(os.path.join(ddir, 'unmatched_clms1.pk'))


####################################
#### Procedure 2
## match my Insured_name and city
'''
	CREATE VIEW  WORK.V_TBL_1 AS
		SELECT UPPER(COMPRESS(t1.Insured_Name,',.#; :"/-@!$%&*+_')) as CLN_Insured_Nm1, 
			   UPPER(COMPRESS(t1.City,',.#; :"/-@!$%&*+_')) as CLN_Loss_City,
				*
		FROM mylib.unmatched_1 t1; 
	
	CREATE VIEW  WORK.V_TBL_2 AS
		SELECT UPPER(COMPRESS(t2.Insured_Nm,',.#; :"/-@!$%&*+_')) as CLN_Insured_Nm2,
			   UPPER(COMPRESS(t2.City_Nm,',.#; :"/-@!$%&*+_')) as CLN_City_Nm,
				*
		FROM HSBINT.epl_policies_18 t2;  
	
	CREATE TABLE mylib.stage_2 AS 
	SELECT * 
	FROM V_TBL_1 as t1
	LEFT JOIN V_TBL_2 as t2
	ON
	(
	    INDEX(TRIM(t1.CLN_Insured_Nm1),TRIM(t2.CLN_Insured_Nm2)) > 0
	    OR 
		INDEX(TRIM(t2.CLN_Insured_Nm2), TRIM(t1.CLN_Insured_Nm1)) > 0
	)
	AND
	TRIM(t1.CLN_Loss_City) = TRIM(t2.CLN_City_Nm)
	AND
	t1.Date_Claim_Reported between t2.policy_eff_dt and t2.policy_exp_dt
	AND
	LENGTH(TRIM(t1.CLN_Insured_Nm1)) > 5
	AND
	LENGTH(TRIM(t2.CLN_Insured_Nm2)) > 5
'''

unmatched_clms1['_ins_nm'] = unmatched_clms1['Insured Name'].str.replace(chars_to_remove,'',regex=True).str.strip().str.upper()
unmatched_clms1['_city'] = unmatched_clms1['City'].str.replace(chars_to_remove,'',regex=True).str.strip().str.upper()
pols['_ins_nm']= pols['Insured_Nm'].str.replace(chars_to_remove,'',regex=True).str.strip().str.upper()
pols['_city']= pols['City_Nm'].str.replace(chars_to_remove,'',regex=True).str.strip().str.upper()

pols_pool = pols[pd.notnull(pols._ins_nm)]
pols_pool = pols_pool[pols_pool._ins_nm.apply(len)>5]
match_tbl = []
for idx,clm in unmatched_clms1.iterrows():
    print(idx)
    if len(clm._ins_nm)<=5: continue
    cond1 = pols_pool._city==clm._city
    cond2 = (pols_pool.policy_eff_dt <= clm['Date Of Loss']) & (pols_pool.policy_exp_dt > clm['Date Of Loss'])  # clm['Date Calim Reported'] replaced
    pols_cand = pols_pool[cond1 & cond2]
    if pols_cand.shape[0]==0: continue
    pols_cand = pols_cand.iloc[
        np.where(
            [re.search(clm._ins_nm,pol._ins_nm) if len(clm._ins_nm)<len(pol._ins_nm)
            else re.search(pol._ins_nm,clm._ins_nm) for _,pol in pols_cand.iterrows()]
        )[0]
    ].copy()
    if pols_cand.shape[0]>0:
        pols_cand['clm_seqid'] = clm.clm_seqid
        match_tbl.append(pols_cand)


cols_to_remove = ['_ins_nm','_city']
pols = pols.drop(cols_to_remove,axis=1)

if len(match_tbl)>0:
    pols_matched = pd.concat(match_tbl).drop(cols_to_remove,axis=1)
    stage2 = pd.merge(unmatched_clms1, pols_matched, how='left', on='clm_seqid')
    stage2.to_pickle(os.path.join(ddir,'stage2.pk'))

    tmp = stage2[pd.notnull(stage2.pol_seqid)][['clm_seqid','pol_seqid']]
    print(tmp.clm_seqid.drop_duplicates().shape[0] / unmatched_clms1.shape[0]) # hit ratio of second process 0.44
    print(tmp.groupby('clm_seqid').count().sort_values('pol_seqid', ascending=False)[:10])
    matched_2 = stage2[ pd.notnull(stage2.pol_seqid)]
    unmatched_clms2 = unmatched_clms1[ ~unmatched_clms1.clm_seqid.isin(pols_matched.clm_seqid.unique())].copy()

else:
    print('No match')
    matched_2 = pd.DataFrame()
    unmatched_clms2 = unmatched_clms1.copy()

matched_2.to_pickle(os.path.join(ddir,'matched_2.pk'))
unmatched_clms2.to_pickle(os.path.join(ddir,'unmatched_clms2.pk'))


#########################################################################
#### Procedure 3
## match by Insured_name and cc_policy_number

'''    
CREATE VIEW  WORK.V_TBL_1 AS
    SELECT UPPER(COMPRESS(t1.Insured_Name,',.#; :"/-@!$%&*+_')) as CLN_Insured_Nm1, 
           UPPER(COMPRESS(t1.City,',.#; :"/-@!$%&*+_')) as CLN_Loss_City,
            *
    FROM mylib.unmatched_2 t1; 

CREATE VIEW  WORK.V_TBL_2 AS
    SELECT UPPER(COMPRESS(t2.Insured_Nm,',.#; :"/-@!$%&*+_')) as CLN_Insured_Nm2,
           UPPER(COMPRESS(t2.City_Nm,',.#; :"/-@!$%&*+_')) as CLN_City_Nm,
            *
    FROM HSBINT.epl_policies_18 t2; 

CREATE TABLE mylib.stage_3 AS
    SELECT * 
    FROM WORK.V_TBL_1 t1
    LEFT JOIN WORK.V_TBL_2 t2
    ON
        (
            INDEX(
                SUBSTR( COMPRESS(UPPER(t2.Client_Policy),',.#;:"/-@! $%&*+_'), 1,10 ),
                SUBSTR(COMPRESS(UPPER(t1.CC_Policy_Number),',.#;:"/-@! $%&*+_'), 1,10)
            ) > 0
        )		    
        AND		    
        (
            INDEX(TRIM(t1.CLN_Insured_Nm1),TRIM(t2.CLN_Insured_Nm2)) > 0
            OR
            INDEX(TRIM(t2.CLN_Insured_Nm2),TRIM(t1.CLN_Insured_Nm1)) > 0
        )
        AND
        t1.Date_Claim_Reported between t2.Policy_eff_dt and t2.policy_exp_dt
        AND
        LENGTH(TRIM(t1.CLN_Insured_Nm1)) > 5
        AND
        LENGTH(TRIM(t2.CLN_Insured_Nm2)) > 5 
'''


unmatched_clms2['cc_pol_nbr'] = unmatched_clms2['CC Policy Number'].str.replace(chars_to_remove,'',regex=True).str.strip().str.slice(0,10).str.upper()
unmatched_clms2['_ins_nm'] = unmatched_clms2['Insured Name'].str.replace(chars_to_remove, '',regex=True).str.strip().str.upper()
pols['cc_pol_nbr'] = pols['Client_Policy'].str.replace(chars_to_remove,'',regex=True).str.strip().str.slice(0,10).str.upper()
pols['_ins_nm'] = pols['Insured_Nm'].str.replace(chars_to_remove, '', regex=True).str.strip().str.upper()

pols_pool = pols[pd.notnull(pols._ins_nm)]
pols_pool = pols_pool[pols_pool._ins_nm.apply(len)>5]
match_tbl = []
for idx,clm in unmatched_clms2.iterrows():
    print(idx)
    if len(clm._ins_nm)<=5: continue
    cond1 = (pols_pool.policy_eff_dt <= clm['Date Of Loss']) & (pols_pool.policy_exp_dt > clm['Date Of Loss'])  # clm['Date Calim Reported'] replaced
    pols_cand = pols_pool[cond1]
    if pols_cand.shape[0]==0: continue
    pols_cand = pols_cand.iloc[
        np.where(
            [re.search(clm.cc_pol_nbr,pol.cc_pol_nbr) if len(clm.cc_pol_nbr)<len(pol.cc_pol_nbr)
             else re.search(pol.cc_pol_nbr,clm.cc_pol_nbr) for _,pol in pols_cand.iterrows()]
        )[0]
    ]
    if pols_cand.shape[0] == 0: continue
    pols_cand = pols_cand.iloc[
        np.where(
            [re.search(clm._ins_nm,pol._ins_nm) if len(clm._ins_nm)<len(pol._ins_nm)
            else re.search(pol._ins_nm,clm._ins_nm) for _,pol in pols_cand.iterrows()]
        )[0]
    ].copy()
    if pols_cand.shape[0]>0:
        pols_cand['clm_seqid'] = clm.clm_seqid
        match_tbl.append(pols_cand)

cols_to_remove = ['_ins_nm','cc_pol_nbr'] # remove _city later
pols = pols.drop(cols_to_remove,axis=1)

if len(match_tbl)>0:
    pols_matched = pd.concat(match_tbl).drop(cols_to_remove,axis=1)
    stage3 = pd.merge(unmatched_clms2, pols_matched, how='left', on='clm_seqid')
    stage3.to_pickle(os.path.join(ddir,'stage3.pk'))

    tmp = stage3[pd.notnull(stage3.pol_seqid)][['clm_seqid','pol_seqid']]
    print(tmp.clm_seqid.drop_duplicates().shape[0] / unmatched_clms2.shape[0]) # hit ratio of second process 0.44
    print(tmp.groupby('clm_seqid').count().sort_values('pol_seqid', ascending=False)[:10])

    matched_3 = stage3[ pd.notnull(stage3.pol_seqid)]
    unmatched_clms3 = unmatched_clms2[ ~unmatched_clms2.clm_seqid.isin(pols_matched.clm_seqid.unique())]
else:
    print('No match')
    matched_3 = pd.DataFrame()
    unmatched_clms3 = unmatched_clms2.copy()

matched_3.to_pickle(os.path.join(ddir,'matched_3.pk'))
unmatched_clms3.to_pickle(os.path.join(ddir,'unmatched_clms3.pk'))


#####################################################
#### Procedure 3
## match by cc_policy_number and city
'''

	CREATE VIEW  WORK.V_TBL_1 AS
		SELECT UPPER(COMPRESS(t1.Insured_Name,',.#; :"/-@!$%&*+_')) as CLN_Insured_Nm1, 
			   UPPER(COMPRESS(t1.City,',.#; :"/-@!$%&*+_')) as CLN_Loss_City,
				*
		FROM mylib.unmatched_3 t1; 
	
	CREATE VIEW  WORK.V_TBL_2 AS
		SELECT UPPER(COMPRESS(t2.Insured_Nm,',.#; :"/-@!$%&*+_')) as CLN_Insured_Nm2,
			   UPPER(COMPRESS(t2.City_Nm,',.#; :"/-@!$%&*+_')) as CLN_City_Nm,
				*
		FROM HSBINT.epl_policies_18 t2; 

	CREATE TABLE mylib.stage_4 AS
		SELECT 	* 
		FROM WORK.V_TBL_1 t1
		LEFT JOIN WORK.V_TBL_2 t2
		ON 	(INDEX(SUBSTR(COMPRESS(UPPER(t2.Client_Policy),',.#;:"/-@! $%&*+_'),1,10),SUBSTR(COMPRESS(UPPER(t1.CC_Policy_Number),',.#;:"/-@! $%&*+_'),1,10)) > 0)
		    AND
			TRIM(t1.CLN_Loss_City) = TRIM(t2.CLN_City_Nm)
			AND
			t1.Date_Claim_Reported between t2.Policy_eff_dt and t2.policy_exp_dt	
'''

unmatched_clms3['cc_pol_nbr'] = unmatched_clms3['CC Policy Number'].str.replace(chars_to_remove,'',regex=True).str.strip().str.slice(0,10).str.upper()
unmatched_clms3['_city'] = unmatched_clms3['City'].str.replace(chars_to_remove, '', regex=True).str.strip().str.upper()
#unmatched_clms3['_ins_nm'] = unmatched_clms3['Insured Name'].str.replace(chars_to_remove, '',regex=True).str.strip().str.upper()
pols['cc_pol_nbr'] = pols['Client_Policy'].str.replace(chars_to_remove,'',regex=True).str.strip().str.slice(0,10).str.upper()
pols['_city'] = pols['City_Nm'].str.replace(chars_to_remove, '', regex=True).str.strip().str.upper()
#pols['_ins_nm'] = pols['Insured_Nm'].str.replace(chars_to_remove, '', regex=True).str.strip().str.upper()

pols_pool = pols
match_tbl = []
for idx,clm in unmatched_clms3.iterrows():
    print(idx)
    if len(clm._ins_nm)<=5: continue
    cond1 = pols_pool._city == clm._city
    cond2 = (pols_pool.policy_eff_dt <= clm['Date Of Loss']) & (pols_pool.policy_exp_dt > clm['Date Of Loss'])  # clm['Date Calim Reported'] replaced
    pols_cand = pols_pool[cond1&cond2]
    if pols_cand.shape[0]==0: continue
    pols_cand = pols_cand.iloc[
        np.where(
            [re.search(clm.cc_pol_nbr,pol.cc_pol_nbr) if len(clm.cc_pol_nbr)<len(pol.cc_pol_nbr)
             else re.search(pol.cc_pol_nbr,clm.cc_pol_nbr) for _,pol in pols_cand.iterrows()]
        )[0]
    ].copy()
    if pols_cand.shape[0]>0:
        pols_cand['clm_seqid'] = clm.clm_seqid
        match_tbl.append(pols_cand)


cols_to_remove = ['_city','cc_pol_nbr','_ins_nm']
pols = pols.drop(cols_to_remove,axis=1)

if len(match_tbl)>0:
    pols_matched = pd.concat(match_tbl).drop(cols_to_remove,axis=1)
    stage4 = pd.merge(unmatched_clms3, pols_matched, how='left', on='clm_seqid')
    stage4.to_pickle(os.path.join(ddir,'stage3.pk'))

    tmp = stage4[pd.notnull(stage4.pol_seqid)][['clm_seqid','pol_seqid']]
    print(tmp.clm_seqid.drop_duplicates().shape[0] / unmatched_clms3.shape[0]) # hit ratio of second process 0.44
    print(tmp.groupby('clm_seqid').count().sort_values('pol_seqid', ascending=False)[:10])

    matched_4 = stage4[ pd.notnull(stage4.pol_seqid)]
    unmatched_clms4 = unmatched_clms3[ ~unmatched_clms3.clm_seqid.isin(pols_matched.clm_seqid.unique())]
else:
    print('No match')
    matched_4 = pd.DataFrame()
    unmatched_clms4 = unmatched_clms3.copy()


matched_4.to_pickle(os.path.join(ddir, 'matched_4.pk'))
unmatched_clms4.to_pickle(os.path.join(ddir,'unmatched_clms4.pk'))


##############################################
#### Wrapping-up
## by merging matched_{1,2,3,4} and leaving umatched_clms4

matched = pd.concat([matched_1,matched_2,matched_3,matched_4], axis=1, ignore_index=True)
matched.to_pickle(os.path.join(ddir, 'matched_final.pk'))
unmatched_clms4.to_pickle(os.path.join(ddir, 'unmatched_final.pk'))


